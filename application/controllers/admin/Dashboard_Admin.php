<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_Admin extends CI_Controller {

	public function index()
	{
		// check_alredy_login();

        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin_view/dashboard_admin_v');
        $this->load->view('templates_admin/footer');
	}

	
}