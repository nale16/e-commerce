<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

    public function index() {
        $data['invoice'] = $this->model_invoice->tampil_data();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin_view/invoice_v', $data);
        $this->load->view('templates_admin/footer');
    }

}