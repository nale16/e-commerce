<div class="container-fluid">
    <h4>Keranjang Belanja</h4>

    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th align="center">No</th>
            <th align="center">Nama Produk</th>
            <th align="center">Jumlah</th>
            <th align="center">Harga</th>
            <th align="center">Sub Total</th>
        </tr>

        <?php 
            $no = 1;
            foreach($this->cart->contents() as $items) :
        ?>

            <tr>
                <td align="center"><?php echo $no++ ?></td>
                <td align="center"><?php echo $items['name'] ?></td>
                <td align="center"><?php echo $items['qty'] ?></td>
                <td align="center">Rp <?php echo number_format($items['price'], 0, ',', '.') ?></td>
                <td align="center">Rp <?php echo number_format($items['subtotal'], 0, ',', '.') ?></td>
            </tr>

        <?php endforeach; ?>

        <tr>
            <td colspan="4"></td>
            <td align="center">Rp <?php echo number_format($this->cart->total(), 0, ',', '.') ?></td>
        </tr>
    </table>

    <div align="right">
        <a href="<?=base_url('Dashboard/hapus_keranjang')?>" class="href"><div class="btn btn-sm btn-danger">Hapus Keranjang</div></a>
        <a href="<?=base_url('Dashboard/index')?>" class="href"><div class="btn btn-sm btn-primary">Lanjutkan Belanja</div></a>
        <a href="<?=base_url('Dashboard/pembayaran')?>" class="href"><div class="btn btn-sm btn-success">Pembayaran</div></a>
    </div>
</div>