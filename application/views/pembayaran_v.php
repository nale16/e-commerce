<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="btn btn-sm btn-success">
                <?php
                $grand_total = 0;
                if($keranjang = $this->cart->contents()) {
                    foreach ($keranjang as $item)
                    {
                        $grand_total = $grand_total + $item['subtotal'];
                    }

                    echo '<h5>Total Belanja Anda : Rp ' . number_format($grand_total, 0, ',', '.');
                  ?>
            </div><br><br>

            <h3>Input Alamat Pengiriman dan Pembayaran</h3>

            <form action="<?=base_url('Dashboard/proses_pesanan') ?>" method="post">
                <div class="form-group">
                    <label for="">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama">
                </div>

                <div class="form-group">
                    <label for="">Alamat Lengkap</label>
                    <input type="text" class="form-control" name="alamat">
                </div>

                <div class="form-group">
                    <label for="">No. Telepon</label>
                    <input type="text" class="form-control" name="no_telp">
                </div>

                <div class="form-group">
                    <label for="">Jasa Pengiriman</label>
                    <select name="" id="" class="form-control">
                        <option value="">JNE</option>
                        <option value="">TIKI</option>
                        <option value="">POS</option>
                        <option value="">GOJEK</option>
                        <option value="">GRAB</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Metode Pembayaran</label>
                    <select name="" id="" class="form-control">
                        <option value="">BCA</option>
                        <option value="">BRI</option>
                        <option value="">Mandiri</option>
                        <option value="">BNI</option>
                    </select>
                </div>

                <button class="btn btn-sm btn-primary mb-3" type="submit">Pesan</button>
            </form>

            <?php
            } else {
                echo "<h4> Keranjang Belanja Anda Masih Kosong </h4>";
            }
            ?>

        </div> 

        <div class="col-md-2"></div>
    </div>
</div>